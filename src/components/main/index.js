import React, { Component } from 'react';
import Title from '../title';
import AddTask from '../add-task';

import './main.css'

class Main extends Component {
    state = { taskArr: []}

    /* Добавление задания в Task List */

    onUpDateTaskArr = (object) => {
        this.setState(
            ({ taskArr}) => {
                const newArr = taskArr;
                newArr.push(object);
                return {
                    taskArr: newArr
                }
            }
        )
    }

    /* Удаление задания с Task List */
    
    onDelTaskArr = (index) => {              
        this.setState(
            ({ taskArr})=>{
                const newArr = taskArr;
                newArr.splice(index,1);
                return {
                    taskArr: newArr
                }
            }
        )
    }    
    
    /* Перечеркиваем задания в Task List */

    onCheckTaskArr = (index) => {   
        this.setState(            
            ({ taskArr}) => {
                const newArr = taskArr;
                if (newArr[index].active === true){
                    newArr[index].active = false
                    return {
                        taskArr: newArr
                    }
                } else if (newArr[index].active === false) {
                    newArr[index].active = true
                    return {
                        taskArr: newArr 
                    }
                }
            }
        )
    }

/*     onFixTaskArr = () => {
        this.setState (
            () => {
                return {

                }
            })
    } */

    render() {
        const { taskArr } = this.state;
        return (
            <div className='main_div'>
                <Title text='Список задач на сегодня'> </Title>
                <AddTask 
                    taskArr={taskArr} 
                    onCheckTaskArr={this.onCheckTaskArr}     /* перечеркиваем задание, задание выполнено */
                    onUpDateTaskArr={this.onUpDateTaskArr}   /* Добавляем задание */
                    onDelTaskArr={this.onDelTaskArr}         /* удаляем задание */
                ></AddTask>
                <Title text={`Осталось  ${taskArr.length}  задач`} onUpDateTaskArr = {this.onUpDateTaskArr}></Title>
            </div >
        )
    }
}
export default Main;