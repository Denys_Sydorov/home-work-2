import React from 'react';
import Input from '../input';
import TaskList from '../task-list';


export default function AddTask({onUpDateTaskArr, onDelTaskArr, onFixTaskArr, onCheckTaskArr, taskArr}){
    return(
        <div>
            <Input onUpDateTaskArr = {onUpDateTaskArr}/>
            <TaskList  taskArr={taskArr} onDelTaskArr={onDelTaskArr} onFixTaskArr={onFixTaskArr} onCheckTaskArr={onCheckTaskArr}></TaskList>
        </div>
    )
} 