import React, { Component } from 'react';

import "./input.css";

class Input extends Component {

    state = {inptInfo:""}
    addText = (text) => {
        this.setState(()=>{
            return this.state.inptInfo = text
        })
    }
    render() {
        const saveBtn = () => {
            const a = this.state.inptInfo.length < 4 ? null : this.props.onUpDateTaskArr({text: this.state.inptInfo, date: new Date(), active: true});
    }    
    return(
        <div>
            <input type='text' className="input-task" onInput={(e)=>{this.addText(e.target.value)}}></input>
            <input type='button' value='Сохранить' className="btn-save" onClick={()=>{saveBtn()}}></input>           
        </div>
    )}    
}

export default Input;