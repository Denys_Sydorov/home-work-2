import React from 'react';

import './task-list.css';

    const TaskList = ({taskArr, onDelTaskArr, onCheckTaskArr, onFixTaskArr}) => {
        return(
            <div>
                <ul>
                    {taskArr.map((task, index) => {
                        return (                                                       
                            <li className={task.active === true ? 'task-list' : 'un-task-list'} key={index * 5 + 'b'}>{task.text},{task.date.getDate()},                 {/* Тернарный оператор, если  active = true применяем обычный класс, если active = faulse тогда применяем класс с перечеркивание текста*/}
                                <div><i onClick={()=>{onCheckTaskArr(index)}} className="fas fa-check check"></i></div>        {/* Перечеркиваем выбраную строку из заданий в массиве taskArr */}
                                <div><i onClick={()=>{onFixTaskArr(task, index)}} className="fas fa-pen fix"></i></div>        {/* Пока ХЗ как делать */}
                                <div><i onClick={()=>{onDelTaskArr(index)}} className="far fa-trash-alt del"></i></div>        {/* Удаляем задание по индексу */}
                            </li>)                            
                    })}                    
                </ul>                
            </div>            
        )
    }   
    
export default TaskList;